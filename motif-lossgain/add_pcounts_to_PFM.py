#!~/bin/python3

## Author: Laura E Cook, University of Melbourne, 7 April 2020
## Purpose: Add pseudocounts to every value in a PFM
## Last update: 7 April 2020


########################################################################################################################
####-------------------------------------------------IMPORT MODULES-------------------------------------------------####
########################################################################################################################

import numpy as np
import sys
import os
import pandas as pd
from Bio import motifs
from Bio.Alphabet import Gapped, generic_dna
from Bio.Alphabet import IUPAC
from Bio import SeqIO

#######################################################################################################################
####------------------------------------------------ADD PSEUDOCOUNTS-----------------------------------------------####
#######################################################################################################################


## Pseudocount file
xls_jaspar = open("jaspar_thresholds2.xlsx", "rb")
jaspar_df = pd.read_excel(xls_jaspar, sheet_name = 0)

# Load files with the position weight matricies
## Set working directory
#os.chdir("/data/projects/punim0586/lecook/motif_lossgain/code/")

os.chdir("/Users/lauracook/phd/OneDrive - The University of Melbourne/motif_analyses/twar_motif_code/motif_lossgain/")


fh = open("jaspar_CORE_ALL.txt")

for m in motifs.parse(fh, "jaspar"):
    # variable to save dataframes in
    # save matrix_id and TF name in a variable
    matrix_id = m.matrix_id
    tf_name = m.name
    #print(m)
    # determine appropriate pseudocount
    m.pseudocounts = motifs.jaspar.calculate_pseudocounts(m)

    # determine pwm
    pwm = m.counts.normalize(pseudocounts=m.pseudocounts)
    print(matrix_id + "_" + tf_name)
    print(pwm)
    print("\n")







# OLD STUFF KEEP FOR FUTURE REFERENCE
#
# #loop through each pfm in the directory
# for m in pfm_file_names:
#     #print(m)
#     #save each pfm as a numpy array (need to do this so that I can add pseudocounts to the matrix)
#     #matrix_id = jaspar_df['matrix_id']
#     #print(data)
#     for i in jaspar_df['matrix_id']:
#         if m.startswith(i + "_"):
#             data = np.genfromtxt(m, delimiter=",")
#             for p in jaspar_df['pseudocount']:
#                 data2 = data + p
#                 print(m)
#                 print(data2)
#
#     #for matrix in matrix_id:
#     #pseudocount = jaspar_df['pseudocount']
#     #print(pseudocount)
#
#     #Save PWMs files that are present in the threshold dataset
# #     pwf_fn = [fn0 for fn0 in pfm_file_names if fn0.startswith(matrix_id + "_")]
# #     print(fn0)
# #     ## Save just the TF name to a variable
# #     tf0 = pwf_fn[0].split("_")[1].split(".")[0]
# #
# #     ## Open each PWM file
# #     #handle_jaspar = open(pwf_fn[0])
# #
# #     # Read motifs
# #     #motf_matrix0 = motifs.read(handle_jaspar, "jaspar")
# #
# #     # Add pseudocount to each matrix
# #     #motf_matrix1 = motf_matrix0 + pseudocount
# #
# #     #print(motf_matrix1)
