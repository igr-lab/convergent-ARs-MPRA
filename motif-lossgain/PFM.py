'''
Created on Oct 26, 2018

@author: Elliott
'''
# from Bio import motifs
# from Bio.motifs import _pwm
class PFM(object):
    '''
    pfm0 = PFM(pssm0, threshold_3e, tf0, pwf_fn)
    '''

    def __init__(self, motif_matrix0, threshold0, transcription_factor0, file_name0):
        '''
        Constructor
        '''
        self.pssm = motif_matrix0.pssm
        self.threshold = threshold0
        self.transcription_factor = transcription_factor0
        self.file_name = file_name0
        self.width = len(motif_matrix0)


    def get_width(self):
        return self.width


    def get_pssm(self):
        return self.__pssm


    def get_threshold(self):
        return self.__threshold


    def get_transcription_factor(self):
        return self.__transcription_factor


    def get_file_name(self):
        return self.__file_name


    def set_pssm(self, value):
        self.__pssm = value


    def set_threshold(self, value):
        self.__threshold = value


    def set_transcription_factor(self, value):
        self.__transcription_factor = value


    def set_file_name(self, value):
        self.__file_name = value


    def del_pssm(self):
        del self.__pssm


    def del_threshold(self):
        del self.__threshold


    def del_transcription_factor(self):
        del self.__transcription_factor


    def del_file_name(self):
        del self.__file_name

    pssm = property(get_pssm, set_pssm, del_pssm, "pssm's docstring")
    threshold = property(get_threshold, set_threshold, del_threshold, "threshold's docstring")
    transcription_factor = property(get_transcription_factor, set_transcription_factor, del_transcription_factor, "transcription_factor's docstring")
    file_name = property(get_file_name, set_file_name, del_file_name, "file_name's docstring")
