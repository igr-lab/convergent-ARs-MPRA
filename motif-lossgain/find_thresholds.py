#!~/bin/python3

## Author: Laura E Cook, University of Melbourne, 27 March 2020
## Purpose: Find score thresholds for each JASPAR motif. Use the score thresholds
## as a cut off for loss or gain analysis
## Last update: 6 April 2020

## Usage:
## Run on spartan server: ssh lecook@spartan.hpc.unimelb.edu.au

########################################################################################################################
####-------------------------------------------------IMPORT MODULES-------------------------------------------------####
########################################################################################################################

from Bio import motifs
from Bio.Alphabet import Gapped, generic_dna
from Bio.Alphabet import IUPAC
from Bio import SeqIO
import sys
import os
import pandas as pd

#######################################################################################################################
####------------------------------------------------FIND THRESHOLDS------------------------------------------------####
#######################################################################################################################


os.chdir("/data/projects/punim0586/lecook/motif_lossgain/hocomoco_PWM/")

# input file: single text file with all motifs in jaspar format
fh = open("all.txt")
#fh = open("test.txt")

# output file: write to this in loop
fn_out = "hocomoco_thresholds.txt"

# column order for output
col_order = [ "matrix_id", "tf_name", "pseudocount", "max_score", "min_score", "mean_score", "standard_deviation", "threshold_fpr001", "threshold_fpr0001", "threshold_balanced"]

# use the uniform background model
background = {"A":0.25,"C":0.25,"G":0.25,"T":0.25}

full_df = []

## loop through motifs
for m in motifs.parse(fh, "jaspar"):
    # variable to save dataframes in
    # save matrix_id and TF name in a variable
    matrix_id = m.matrix_id
    print(matrix_id)
    tf_name = m.name
    print(tf_name)

    # determine appropriate pseudocount
    m.pseudocounts = motifs.jaspar.calculate_pseudocounts(m)

    # add pseudocount to motif PFM and save output
    #p = m.pseudocounts['G']

    # determine pwm
    pwm = m.counts.normalize(pseudocounts=m.pseudocounts)

    print(matrix_id + "_" + tf_name)
    print(pwm)
    print("\n")

    # determine pssm
    pssm = pwm.log_odds(background)

    # determine max
    max = pssm.min

    # determine min
    min = pssm.max

    # determine mean: The mean is particularly important, as its value is equal to the Kullback-Leibler divergence or relative entropy,
    #and is a measure for the information content of the motif compared to the background
    mean = pssm.mean(background)

    #determine standard deviation
    std = pssm.std(background)

    # determine the distribution of the pssm with default precision (not sure what that means)
    distribution = pssm.distribution(background=background, precision=10**4)

    # threshold approach using false positive rate 0.001
    threshold_fpr001 = distribution.threshold_fpr(0.001)

    # threshold approach using false positive rate 0.0001
    threshold_fpr0001 = distribution.threshold_fpr(0.0001)
    #
    # threshold using balanced approach (satisfies some relation between the false-positive rate and false-negative rate (fnr/fpr=t))
    threshold_balanced = distribution.threshold_balanced(1000)

    data = {'matrix_id': [str(matrix_id)], 'tf_name': [tf_name], 'pseudocount':[str(m.pseudocounts['G'])], 'max_score': [str(max)], 'min_score':[str(min)],'mean_score':[str(mean)], 'standard_deviation': [str(std)], 'threshold_fpr001':[str(threshold_fpr001)], 'threshold_fpr0001': [str(threshold_fpr0001)], 'threshold_balanced': [str(threshold_balanced)]}
    df = pd.DataFrame(data, columns = ['matrix_id', 'tf_name', 'pseudocount', 'max_score', 'min_score', 'mean_score', 'standard_deviation', 'threshold_fpr001', 'threshold_fpr0001', 'threshold_balanced'])
    full_df.append(df)

df = df.append(full_df, True)
df.to_csv(path_or_buf = fn_out, sep = "\t", header =True)
