# Functional characterisation of convergent ARs #
This repository contains all of the code used to prepare sequences for generating an MPRA library and analysing the output sequencing data. Each folder has it's own README outling the analyses.

# Convergent ARs #
Convergent ARs (or TWARs= Thylacine and Wolf Accelerated Regions) were desribed by Feigin et al 2019 Genome Research. A brief overview of the analysis describing ARs is below. 

**PHAST (Phylogenetic Analysis with Space/Time Models) PACKAGE:**
- **phastCons:** conservation scoring and identification of conserved elements
- **phyloFit:** fitting of phylogenetic models to aligned DNA sequences
- **phyloP:** computation of p-values for conservation or acceleration, either lineage-specific or across all branches <br><br>

***

# Data Preparation for phastCons

**MULTIPLE ALIGNMENT**
                                                                            
59 vertebrate whole genome alignments against mouse (maf format) 
> http://hgdownload.cse.ucsc.edu/goldenPath/mm10/multiz60way/maf/

Corresponding phyloP tree model 
> http://hgdownload.cse.ucsc.edu/goldenpath/mm10/phyloP60way/

Positional information for Thylacine and Wolf taken from maf alignment headers of Tassive Devil and dog. <br>
Homologous regions of the thylacine and wolf genomes extracted from Tassie Devil and dog using **samtools faidx**
> samtools faidx <br>
Index reference sequence in the FASTA format or **extract subsequence from indexed reference sequence**. If no region is specified, faidx will index the file and create <ref.fasta>.fai on the disk. If regions are specified, the **subsequences will be retrieved and printed to stdout in the FASTA format**. The input file can be compressed in the BGZF format.<br>
This only works in this case because the thylacine genome is a reference-based assembly to the devil genome, meaning that the coordinate system is the same. This allows you to then insert the thylacine genome into the pre-computed multiple alignment because the devil is already in that alignment. The same was done in regards to the wolf and dog genomes. The dog genome used to generate the wolf reference-based assembly is CanFam3.1 (Hoeppner et al. 2014), an improved assembly compared with the original dog genome (Lindblad-Toh et al. 2005). Because the domestic dog has likely experienced different selective pressures compared with wild canids such as the wolf, it was stripped from all MAF alignments.

Thylacine and Wolf homologous regions projected into the maf subalignment using **MAFFT**<br>
Domestic dog removed from all maf alignments and new 61-way alignment (60 vertebrates against mouse) used for the analyses.
All of this is in charlies script.

> *pathway for files* = /Volumes/Seagate Backup Plus Drive/TWARs/analysis/mafs/intermediates

For each subalignment, individual sequences were **removed** if they contained greater than **50%** gap or ambiguous characters.

Subalignments with **fewer than 30** species (including wolf and thylacine) were **removed**

> *scripts* = filter_maf_seqs_by_percent_ambiguous.pl and filter_maf_by_species_number.pl <br>
*pathway for filtered files* = /Volumes/Seagate Backup Plus Drive/TWARs/analysis/mafs/filtered

**GENERATING A PHYLOGENETIC MODEL FOR phastCons INPUT**

The pre-computed phyloP tree model in the original 60-way whole genome alignments was modified to include thylacine and replace domestic dog with wolf. **phastCons** needs phylogenetic models to run.

**Non-conserved model:**<br>
Annotations of mouse coding DNA sequences downloaded from Ensembl and filtered down to the set of longest complete sequences for each gene. <br>
> The nonconserved model is essentially estimated from sites outside of predicted conserved elements. If a data set contains very distant species, which align mostly in conserved regions (e.g., coding exons), then the estimates of the nonconserved branch lengths to these species will tend to be underestimated, because any "nonconserved" bases that do align are probably actually at least partially conserved. It may make sense in such a case to estimate a nonconserved model from 4d sites in coding regions (where alignments can reliably be obtained), and by letting phastCons estimate the parameter rho. <br>
This is also recommended (over letting phastCons itself estimate the models directly from the data) when you have more than 12 species and many distant species with low alignment coverage, and you are concerned about distortions in branch lengths.

4 degenerate sites (third codon positions) are called using the annotated sequences to estimate the nonconserved model.
**Sufficient statistics (.ss)** files were generated from whole genome alignments using **third codon positions** in the filtered Ensembl mouse coding sequence annotations and aggregated with **msa_view**.<br>

> msa_view <br> can extract the **sufficient statistics** for phylogenetic analysis from an alignment, optionally accounting for site categories that are defined by an auxiliary **annotations file**.  Supports various other functions, including gap stripping, column randomization, and reordering of sequences.  Capable of reading and writing in a few common formats.  Can be used for file conversion (by default, output is the entire input alignment).

**Four-fold degenerate sites:**

> A position of a codon is said to be a four-fold degenerate site if any nucleotide at this position specifies the same amino acid. For example, the third position of the *glycine* codons (GGA, GGG, GGC, GGU) is a four-fold degenerate site because all nucleotide substitutions at this site are synonymous. 


**phyloFit** was used estimate the models and to recompute the tree model based on the collected sufficient statistics. <br>
Resulting tree model was highly consistent with the original 60-way model provided by UCSC Genome Browser. 


> **phyloFit** <br>
Fits one or more tree models to a multiple alignment of DNA sequences by maximum likelihood, using the specified tree topology and substitution model. A description of each model will be written to a separate file, with the suffix ".mod". These .mod files minimally included a substitution rate matrix, a tree with branch lengths and estimates of nucleotide equilibrium frequencies. They may also include information about parameters for modeling rate variation.


> *pathway to .mod files* = /Volumes/Seagate Backup Plus Drive/TWARs/analysis/models


<div class="alert alert-info"> 
<b>LAURA:</b> My understanding of this analysis is that to be able to use phastCons we need a multiple alignment and a phylogenetic model for the non-conserved regions. <br> *Step 1.* The multiple alignment is generated by doing a 61 way alignment against the mouse and adding in the Thylacine and Wolf homologous regions. <br> *Step 2.* The non-conserved phylogenetic model is generated by using the mouse annotated coding-DNA sequences to call 4 degenerate sites for each of the alignments (msa_view) and then combining all the 4d sites (msa_view --aggregate). <br> *Step 3.* phyloFit then uses both the sufficient statistics and the alignment to estimate the phylogenetic model <br>  </div>

***                                        


# Vertebrate Conserved Elements

Evolutionary conserved regions in the selected vertebrates were identified using **phastCons**. 

> **phastCons** <br>
Program for identifying evolutionarily conserved elements in a multiple alignment, given a phylogenetic tree. It uses statistical models of nucleotide substitution that allow for multiple substitutions per site and for unequal rates of substitution between different pairs of bases (eg. a higher frequency of transitions than transversions). It is based on a statistical model of sequence evolution called a phylogenetic hidden Markov model. The basic idea of the program is to scan along the alignment for regions that are better "explained" by the conserved model than by the nonconserved model; these regions will be output as conserved elements, and the probability that each base is in such a region will be ouput as the conservation score for that base. <br><br>
**input:** multiple alignment, a phylogenetic model for conserved regions (optional) and a phylogenetic model for nonconserved regions. <br>
**output:** base-by-base conservation scores (as displayed int he conservation tracks in the UCSC browser), prediction of discrete conserved elements, estimates free parameters

The following **state transition** parameters were used:
- --expected-length 200
> **expected length =** the expected length of a conserved element.<br><br>
**200 bp** was chosen as **exons** should be almost entirely contiguous stretches of conserved sequence. Enhancers predicted in other studies have an average length of 750-800bp, but are not entirely composed of contiguous stretches of conserved sequences - they are typically composed of multiple, shorter conserved segments of 10-100bp (Visel *et al* 2009, Lee *et al* 2011). <br>
Lower smoothing implies shorter, more fragmented conserved elements, containing fewer nonconserved sites. Increasing the expected length parameter tends to increase the level of smoothing and the average length of predicted conserved elements.

- --target-coverage 0.15
> **expected coverage =** the expected converage by conserved elements. ie. the fraction of bases that would be conserved if the model were used to generate the data.<br><br>
**0.15** was chosen as it represents higher estimates of the percent of the genome that is evolutionarily constrained (conserved) (Ponting and Hardison, 2011, *What fraction of the Human genome is functional?*) <br>


The following link has a useful description on tuning parameters:
> http://compgen.cshl.edu/phast/phastCons-HOWTO.html


Prior to running phastCons, thylacine and wolf sequences were masked from the 61-way alignments. This was done to avoid accelerated evolution in either species from hindering the detection of conservation among vertebrates.


## Mouse Craniofacial ChIP data
The conserved elements predicted by phastCons were then intersected with publically available mouse craniofacial ChIP data for H3K9ac and K3K27ac. Only conserved bases overlapping these markers were retained, yielding an initial set of regions with high potential for cis-regulatory function. <br>
Adjacent conserved regions were merged so that the average percent of all merged regions that was composed of non-conserved bases did not exceed 10%, and that protein-coding exons from our CDS annotation set were on average covered by less than 2 contiguous, conserved segments.<br>
Conserved elements overlapping exons by more than 25% of their length were removed to restrict data to non-coding elements. 
> script = optimize_merge_distances3.2.pl

In total, 40,547 thylacine and 56,278 wolf orthologs of pcfCREs (putative craniofacial Cis-regulatory elements) were represented.                                                                                               

# Thylacine and Wolf Accelerated Regions

                                                                                                            
To identify conserved elements that have undergone accelerated evolution in the thylacine and wolf the likelihood ratio test for acceleration was implemented in **phyloP**. 

> **phyloP** <br>             http://compgen.cshl.edu/phast/help-pages/phyloP.txt <br>
The phylogenetic model must be in the .mod format produced by the phyloFit program.  The alignment file can be in any of several file formats (see --msa-format). <br> Compute conservation or acceleration p-values based on an alignment and a model of neutral evolution.  Will also compute p-values of conservation/acceleration in a subtree and in its complementary supertree given the whole tree (see --subtree).  P-values can be produced for entire input alignments (the default), pre-specified intervals within an alignment (see --features), or individual sites (see --wig-scores and --base-by-base).<br><br> **positive scores** = measure conservation, which is slower evolution than expected, at sites that are predicted to be conserved. <br> **negative scores** = measure acceleration, which is faster evolution than expected, at sites that are predicted to be fast-evolving. <br> <br>

Thylacine and wolf sequences from subalignments corresponding to the *putative craniofacial CREs* were unmasked and then tested with phyloP (parameters: --mode AAC --method LRT) for acceleration in thylacine and wolf.

> **--mode ACC** = compute one-sided p-values so that small p indicates unexpected acceleration.<br><br>
**--method LRT** = method used to compute p-values or conservation/acceleration scores. The likelihood ratio test (LRT) compares an alternative model having a free scale parameter with the given neutral model.

This test returned **12890** alignments with significant p-values (<0.05) for acceleration in thylacine and **3644** elements in wolf. <br>
False detection rate (FDR) correction was performed using **Benjamini-Hochberg** with the FDR set at **10%**.

After the correction **10910** thylacine and **1923** wolf elements were retained. An intersection of the accelerated non-coding putative craniofacial CREs in thylacine and wolf produced a subset of **339** elements (TWARS).

## Testing for GCbias and positive selection
GC-biased gene conversion (gBGC) can confound evolutionary rates tests for positive selection by increasing the fixation rate of GC-rich alleles via biochemical bias during recombination. 

To assess rates of gBGC in the thylacine and wolf, the R package rphast was used (Hubisz et al. 2011) <br>
Accelerated regions in each species were tested using two additional likelihood ratio tests: (1) comparing a model with a parameter for gBGC on the foreground branch against the neutral model, and (2) comparing a model with parameters for both gBGC and selection on the foreground branch (gBGC + Sel) against the model of foreground gBGC alone.

173 TWARs, were retained as evolving adaptively (i.e., driven by selection alone or by selection and gBGC together). Those that were driven by selection or by selection and gBGC in only the thylacine or wolf were also kept in the MPRA experimental design (need to confirm with Charlie that this is the 339)