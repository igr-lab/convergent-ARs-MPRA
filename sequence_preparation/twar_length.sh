#!bin/bash

## Laura E Cook, University of Melbourne
## 5 Oct 2018
## Take a mutiple sequence fasta file and outputs the length of each of the sequences in that fasta


TRA=($(for file in *.fa; do echo $file |cut -d "." -f 1-2;done))

echo ${TRA[@]}

for tr in ${TRA[@]};

do

echo ${tr}

cat ${tr}.fa | awk '$0 ~ ">" {print c; c=0;printf substr($0,2,100) "\t"; } $0 !~ ">" {c+=length($0);} END { print c; }' > ${tr}.txt

done
