#!/bin/bash

### Purpose: Cleans up MAF files in a loop ready to be converted to FASTA

### Author: Laura E Cook, University of Melbourne, 29/11/2019
### Last update: 05/02/2020

TRA=($(for file in TWAR*.chr*.maf; do echo $file |cut -d "." -f 1-2;done))

echo ${TRA[@]}

for tr in ${TRA[@]};

do

echo ${tr}

# when run on MAC OS need to add '' after each sed -i command as MAC OS sed errors without it (BSD)
sed -i '/##maf version/d' ${tr}.maf
sed -i '/maf_parse/d' ${tr}.maf
sed -i '/a score/d' ${tr}.maf
sed -i '/#eof/d' ${tr}.maf

done
