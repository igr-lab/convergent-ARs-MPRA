
#!/bin/bash

### Author: Laura E Cook, Unversity of Melbourne, 25/03/2019
### Last update: 25/03/2019
### Purpose: Count number of orthologues in the TWAR file and report


TRA=($(for file in *.fa; do echo $file |cut -d "." -f 1-2 ;done))

echo ${TRA[@]}

for tr in ${TRA[@]};

do

echo ${tr}

grep ">" ${tr}.fa | wc -l

done
