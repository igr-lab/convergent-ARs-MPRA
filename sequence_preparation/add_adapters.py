#!usr/bin/env python3

## Author: Laura E Cook, Unversity of Melbourne
## Purpose: Add adapter sequences for cloning in MPRA experiment to all tiled sequences in the library

## Usage: python3 add_adapters.py all_TWARs_filtered_BsiWI_tiled.fa > all_TWARs_filtered_BsiWI_tiled_adapters.fa

import string
import random
import sys
import os
from Bio import SeqIO


file = sys.argv[1]
adapter_5='ACTGGCCGCTTGACG'
adapter_3='CACTGCGGCTCCTGC'

# Loop through all the files in the variable
with open(file, 'r') as all_TWARs:
	for record in SeqIO.parse(all_TWARs, "fasta"):
		print(">" + str(record.id))
		print(adapter_5 + str(record.seq) + adapter_3)
all_TWARs.close()