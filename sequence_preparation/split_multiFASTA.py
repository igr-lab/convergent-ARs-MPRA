#!usr/bin/env python3

## Author: Laura E Cook, Unversity of Melbourne, 28/22/2019
## Last update: 06/02/2020
## This is better/cleaner than the bash file as it doesn't skip the last fasta file
## in the file.
## 1. Takes a directory full of multi-FASTA files
## 2. Splits them and saves new FASTA with filename = fasta header

# usage
    ## python3 split_multiFASTA.py <directory path for fasta files>
# example
    ## python3 split_multiFASTA.py concatenated_fasta/

##-------------------------------------------- Import programs -------------------------------------------------------------##

import sys
import os
from Bio import SeqIO

##--------------------------------------------- Run script -----------------------------------------------------------------##

# get the directory from command line and save it as a variable
dir_path = sys.argv[1]

# Loop through each file in the directory
for file in os.listdir(dir_path):

    # if the file is a FASTA file
    if file.endswith('fa'):

        # then save the file path to a variable
        filenames = dir_path + file

        # Loop through all the files in the variable
        with open(filenames, 'r') as f_open:

            # for each record (TWAR header) in the file parse it as a FASTA
            for record in SeqIO.parse(f_open, "fasta"):

                # set the record ID to a variable
                id = record.id

                # set the TWAR sequence to a variable
                seq = record.seq
                print(id)
                # this is the output file
                # open an output file to write to and save it with the ID name + .fa
                id_file = open(id+".fa", "w")

                # write the fasta header and sequence to the new file
                id_file.write(">"+str(id)+"\n"+str(seq)+"\n")

                #important to close the file before beginning the loop again
                id_file.close()
        #close the input file
        f_open.close()
