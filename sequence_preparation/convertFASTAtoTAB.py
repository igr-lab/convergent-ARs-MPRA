#!/usr/bin/env python

### Author: Laura E Cook, University of Melbourne, 07/07/2020
### Purpose: Convert fasta to TAB for generating oligo synthesis spreadsheets

# usage: python3 convertFASTAtoTAB.py sequences.fasta sequences.tsv
# file extension must be tsv to be able to open in Excel

import sys
from Bio import SeqIO

fasta = sys.argv[1]
tabfile = sys.argv[2]

records = SeqIO.parse(fasta, "fasta")
count = SeqIO.write(records, tabfile, "tab")
print("Converted %i fasta sequences" % count)
