#!/bin/bash

## Author: Laura E Cook, Unversity of Melbourne, 20 Jan 2020

## Purpose: This script extracts the aligned sequence for each TWAR in every species in the alignment
## Ideally it would be great to learn how to do this is a loop
## I could generate a txt file with the species genome names and then loop over those names?
## Look at make this more efficient and soft code when I come back to it


TRA=($(for file in TWAR*.chr*.maf; do echo $file |cut -d "." -f 1-2;done))

echo ${TRA[@]}

for tr in ${TRA[@]};

do

echo ${tr}


# grep mm10 ${tr}.maf > species/mm10_${tr}.maf
# grep rn6 ${tr}.maf > species/rn6_${tr}.maf

#grep perManBai1 ${tr}.maf > species/perManBai1_${tr}.maf
#grep criGri1 ${tr}.maf > species/criGri1_${tr}.maf


# grep speTri2 ${tr}.maf > species/speTri2_${tr}.maf
# grep cavPor3 ${tr}.maf > species/cavPor3_${tr}.maf
# grep oryCun2 ${tr}.maf > species/oryCun2_${tr}.maf
#grep ochPri3 ${tr}.maf > species/ochPri3_${tr}.maf
grep Tcyn ${tr}.maf > tcyn_${tr}.maf
grep Clup ${tr}.maf > clup_${tr}.maf
grep macEug2 ${tr}.maf > macEug2_${tr}.maf
grep oviAri1 ${tr}.maf > oviAri1_${tr}.maf
# grep ochPri2 ${tr}.maf > species/ochPri2_${tr}.maf
#grep hg38 ${tr}.maf > species/hg38_${tr}.maf
# grep hg19 ${tr}.maf > species/hg19_${tr}.maf
#grep rheMac8 ${tr}.maf > species/rheMac8_${tr}.maf
# grep rheMac3 ${tr}.maf > species/rheMac3_${tr}.maf
# grep otoGar3 ${tr}.maf > species/otoGar3_${tr}.maf
#grep susScr11 ${tr}.maf > species/susScr11_${tr}.maf
# grep susScr3 ${tr}.maf > species/susScr3_${tr}.maf
#grep HLoviAri4 ${tr}.maf > species/HLoviAri4_${tr}.maf
#grep bosTau8 ${tr}.maf > species/bosTau8_${tr}.maf
# grep bosTau7 ${tr}.maf > species/bosTau7_${tr}.maf
#grep HLequCab3 ${tr}.maf > species/HLequCab3_${tr}.maf
# grep equCab2 ${tr}.maf > species/equCab2_${tr}.maf
#grep felCat8 ${tr}.maf > species/felCat8_${tr}.maf
# grep felCat5 ${tr}.maf > species/felCat5_${tr}.maf
grep ailMel1 ${tr}.maf > ailMel1_${tr}.maf
# grep myoLuc2 ${tr}.maf > species/myoLuc2_${tr}.maf
#grep sorAra2 ${tr}.maf > species/sorAra2_${tr}.maf
# grep sorAra1 ${tr}.maf > species/sorAra1_${tr}.maf
#grep conCri1 ${tr}.maf > species/conCri1_${tr}.maf
# grep loxAfr3 ${tr}.maf > species/loxAfr3_${tr}.maf
# grep triMan1 ${tr}.maf > species/triMan1_${tr}.maf
# grep monDom5 ${tr}.maf > species/monDom5_${tr}.maf
grep sarHar1 ${tr}.maf > sarHar1_${tr}.maf
#grep HLphaCin1 ${tr}.maf > species/HLphaCin1_${tr}.maf
#grep ornAna2 ${tr}.maf > species/ornAna2_${tr}.maf
# grep ornAna1 ${tr}.maf > species/ornAna1_${tr}.maf
# grep danRer7 ${tr}.maf > species/danRer7_${tr}.maf
# grep anoCar2 ${tr}.maf > species/anoCar2_${tr}.maf
# grep galGal4 ${tr}.maf > species/galGal4_${tr}.maf

done
