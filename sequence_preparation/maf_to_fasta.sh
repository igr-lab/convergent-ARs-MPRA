#!bin/bash

### Author: Laura E Cook, Unversity of Melbourne, 01/02/2019
### Last update: 05/02/2020

### Purpose: This script takes a previously extracted sequence from a maf
### alignment (maf_extract_from_WGA.sh)
### 1. Keeps the sequence only (column 7)
### 2. Removes any new line
### 3. Adds a fasta header for each TWAR to the file
### 4. The ouput is the sequence in fasta format

TRA=($(for file in *.maf; do echo $file |cut -d "." -f 1-2;done))

echo ${TRA[@]}

for tr in ${TRA[@]};

do

echo ${tr}

cat ${tr}.maf | grep "^s" | awk '{print $7; }' | tr -d '\n' | awk -v var="${tr}" '{print ">" var "\n" $0}' > ${tr}.fa

done
