#!/usr/bin/python3

## Purpose: Script generates n sequences with random combination of nucleotides,
## checks whether any of these sequences aligns to the mm10 (via blat)
## uses fimo meme to remove sequences with TFBSs
## and then adds the adapters and write down the files

## module load gcccore/8.3.0
## module load blat/3.5
## module load foss/2019b
## module load biopython/1.75-python-3.7.4
## module load meme/5.1.1-python-3.7.4


import pandas as pd
import string
import random
import os
import subprocess
from Bio import SeqIO
from Bio import SearchIO

wd= '/data/projects/punim0586/lecook/functional_validation_TWARs/sequence_preparation/'
blat_dir= wd + 'mm10annotations/'
MPRA_dir= wd +'oligo_library/controls/'
non_filtered_dir= wd +'oligo_library/tmp/'
fimo_dir= wd+ 'oligo_library/tmp/'
meme_file= wd+ 'mm10annotations/'

#make a new function that generates sequences with the nucleotide specified
nucleotides='ATCG'
def sequence_generator(size=1, chars=nucleotides):
    return ''.join(random.choice(chars) for _ in range(size))

# adapter sequences - need to add before checking for TFBS and enzyme sites
adapter_5='ACTGGCCGCTTGACG'
adapter_3='CACTGCGGCTCCTGC'

## generate random sequences that are 170bp and add adapters
random_sequences=[]
for _ in range(10000):
    random_sequences.append(sequence_generator(105))

Name=['random']*10001
ID=list(range(1, 10001))
sequence_identifiers=[ n + '_' + str(i) for n,i in zip(Name, ID)]

random_sequences_df = pd.DataFrame(
    {'sequence_name': sequence_identifiers,
     'sequences': random_sequences
    })

## use BLAT to make sure they aren't found in the mouse genome
subprocess.call(['blat', blat_dir+'mm10.fa', non_filtered_dir +'random_sequences.fa', '-fastMap', non_filtered_dir +'output.psl','-t=dna','-q=dna'])
SearchIO.read(non_filtered_dir+ 'output.psl', 'blat-psl') ## if it outputs: 'No query results found in handle' it means blat hasnt found any matches

# # run meme fimo module load MEME/5.0.5-intel-2018.u4-Perl-5.26.2-Python-3.6.4
os.chdir(fimo_dir)
subprocess.call(['fimo','--bfile','--motif--', meme_file+'HOCOMOCOv11_core_MOUSE_mono_meme_format.meme',non_filtered_dir+'random_sequences.fa'])
os.chdir(wd)

## read the seq column reported on the gff file
fimo_output=pd.read_csv(fimo_dir+'fimo_out/fimo.tsv', delimiter = '\t')
del fimo_output['motif_alt_id']
fimo_output=fimo_output.dropna()
fimo_output=fimo_output['sequence_name'].drop_duplicates()
fimo_output


random_sequences_df
sequences_to_keep=random_sequences_df['sequence_name']
sequences_to_keep=sequences_to_keep[~sequences_to_keep.isin(fimo_output)]
sequences_to_keep = pd.merge(sequences_to_keep, random_sequences_df, how='inner', on=['sequence_name'])

sequences_with_BsiWI=sequences_to_keep.loc[sequences_to_keep['sequences'].str.contains('CGTACG', case=False)]
sequences_with_SfiI=sequences_to_keep.loc[sequences_to_keep['sequences'].str.contains('GGCC[ATCG]{1,5}CCGG', case=False)]

sequences_without_enzymes=sequences_to_keep[~sequences_to_keep.isin(sequences_with_BsiWI)].dropna()
sequences_without_enzymes=sequences_without_enzymes[~sequences_without_enzymes.isin(sequences_with_SfiI)].dropna()

sequence_to_test=sequences_without_enzymes.sample(n=150)

sequence_to_test=sequence_to_test['sequences'].tolist()

with open(MPRA_dir+"random_sequences", "w") as final_sequence_file:
    for i in range(len(sequence_to_test)):
        final_sequence_file.write(">" + sequence_identifiers[i] + "\n" +sequence_to_test[i] + "\n")