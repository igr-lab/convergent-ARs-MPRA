#!/bin/bash

## Author: Laura E Cook, Unversity of Melbourne, 25/03/2019
## Purpose: Concatenate the orthologues so that there is one fasta file per TWAR
## Last update: 11/02/2020

input="twars.txt"
while IFS= read -r line

do

echo "$line"

#cat ailMel1_$line.fa clup_$line.fa vulVul_$line.fa aciJub_$line.fa odoRos_$line.fa croCro_$line.fa meerkat_$line.fa parHer_$line.fa pteBra_$line.fa spiGra_$line.fa taxTax_$line.fa > $line.fa

cat tcyn_$line.fa sarHar1_$line.fa ailMel1_$line.fa clup_$line.fa smiCra-tcyn_$line.fa vulVul-croCro_$line.fa > $line.fa
#macEug3_$line.fa macEug2_$line.fa oviAri1_$line.fa oviAri4_$line.fa 

done < "$input"
