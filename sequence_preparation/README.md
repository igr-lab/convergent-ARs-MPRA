# Sequence Preparation for MPRA Experiment #

This document is the computational notes for the steps taken to prepare the TWAR sequences for designing the MPRA experiment.

Since our TWARs appear to drive craniofacial development through alternations to bones derived almost entirely from the neural crest cell lineage, we will examine our TWARs activity in neural crest stem cells (O9-1) and pre-osteoblast cells (MC3T3-E1), both of which possess the capacity to differentiate into osteoblasts and form calcified condensations in vitro. Thus, we can examine the transcriptional activity of our TWARs during the process of osteoblast differentiation.

MPRAs will enable the simultaneous functional analyses of our complete list of TWARs in a single experiment.
We will synthesize orthologues of our refined list of TWARs from our two convergent species (thylacine and wolf), two non-convergent species (Tasmanian devil and Panda) and from two ancestral state reconstructions of the Dasyurmorphia and Carnivora nodes.


## Extract sequences for all 61 species for the given TWAR mm10 coordinates ##

* The aligned region from the mm10 coordinates for each TWAR were extracted for all species using a wrapper script for **maf_parse**.
* Then the specific TWAR sequence for each species was extracted from the alignment using **grep**.
* All of the species are extracted because I will need these later to re-aligned with additional species sequences that are missing from the alignment so as to check the phylogenetic trees. This will be necessary to provide evidence that including sequences that were not present in the original alignment do not dramatically alter the tree and therefore we can predict that particular sequence would not have altered the output of the accelerated regions analysis (Feigin et al 2019, *Genome Research*).

### Extract TWAR coordinates from maf files

`scripts/extract_bed_regions_from_maf.py`

### Extract species from TWAR maf files

`scripts/maf_extract_from_WGA.sh`

#### MAF file cleanup

`scripts/maf_cleanup.sh`

#### Convert to FASTA files

`scripts/maf_to_fasta.sh`

#### Combine TWARs by TWAR ID

Using a txt file with list of TWAR IDs (**twars.txt**) then loop through species to be added.

`scripts/cat_fasta_loop_TWAR.sh`

**Count orthologues in each TWAR file to confirm right number of species**

`scripts/count_orthologues_in_FASTA.sh`

## Align each TWAR for each of the species

**PRANK**

http://wasabiapp.org/software/prank/

*PRANK is a probabilistic multiple alignment program for DNA, codon and amino-acid sequences. It’s based on a novel algorithm that treats insertions correctly and avoids over-estimation of the number of deletion events. In addition, PRANK borrows ideas from maximum likelihood methods used in phylogenetics and correctly takes into account the evolutionary distances between sequences. Lastly, PRANK allows for defining a potential structure for sequences to be aligned and then, simultaneously with the alignment, predicts the locations of structural units in the sequences.*

Generate array commands:

```
TRA=($(for file in TWAR*.fas; do echo $file |cut -d "." -f 1-2;done))

echo ${TRA[@]}

for tr in ${TRA[@]};

do

echo prank -d=${tr}.fa -o=${tr}
```

Run array commands:
`scripts/basic_array_wrapper.slurm`


## Generate ancestral reconstructions

Based on many discussions with Irene, Andrew and Charlie and with the advice of Katie Pollard we have decided to include all TWARs compared to the ancestral state of the carnivorous ancestor on both sides of the therian tree.
This will mean that we don't have to worry too much about the size differences between the eutherian and marsupials as we won't be directly comparing between those sequences.

For the marsupial side we will reconstruct the ancestor of Dasyuromorphia using the following species genomes:
- dunnart (*Sminthopsis crassicaudata*) v1 pseudochromosome reference-assembly (Charlie)
- Tasmanian devil (*Sacophilus harrissi*) sarHar1 (already have sequences)
- quoll (*Dasyurus hallucatus*) assembly by Adnan (Museums Victoria) Dasyurus_hallucatus_10X_01_style1raw.fasta
- thylacine (*Thylacinus cynaocephalus*) Feigin et al 2018 (already have sequences)

For the eutherian side we will reconstruct the ancestor of Carnivora using the following species genomes:
- giant panda (*Ailuropoda melanoleuca*) ailMel1
- wolf (*Canis lupus*) canLup
- Cheetah (*Acinonyx jubatus*) aciJub1_HiC (dnazoo)
- Red fox (*Vulpes vulpes*) VulVul2.2_HiC.fasta (dnazoo)
- Walrus (*Odobenus rosmarus divergens*) Oros_1.0_HiC (dnazoo)
- Giant Sea Otter (*Pteronura brasiliensis*) Pteronura_brasiliensis_HiC.fasta (dnazoo)
- Meerkat (*Suricata suricatta*) meerkat_22Aug2017_6uvM2_HiC.fasta (dnazoo)
- Spotted hyena  (*Crocuta crocuta*) croCro
- Asian Palm Civet (*Paradoxurus hermaphroditus*) parHer
- American badger (*Taxidea taxus jeffersonii*) taxTax
- Western spotted skunk (*Spilogale gracilis*) spiGra

### Get sequences for species in the tree
#### Build BLASTn databases

```
makeblastdb –in dunnart_pseudochr_vs_mSarHar1_v1.fasta –dbtype nucl –parse_seqids &
makeblastdb –in Dasyurus_hallucatus_10X_01_style1raw.fasta –dbtype nucl –parse_seqids &
makeblastdb -in Oros_1.0_HiC.fasta -dbtype nucl -parse_seqids &
makeblastdb -in Pteronura_brasiliensis_HiC.fasta -dbtype nucl -parse_seqids &
makeblastdb -in VulVul2.2_HiC.fasta -dbtype nucl -parse_seqids &
makeblastdb -in meerkat_22Aug2017_6uvM2_HiC.fasta -dbtype nucl -parse_seqids &
makeblastdb -in aciJub1_HiC.fasta -dbtype nucl -parse_seqids &
makeblastdb -in Crocuta_crocuta_HiC.fasta -dbtype nucl -parse_seqids &
```

#### BLAST for TWARs
```
blastn -task blastn -num_threads 4 -db dunnart_pseudochr_vs_mSarHar1.11_v1.fasta -query sarHar1_twars.fa -out sarHar1-vs-dunnart.blast.tsv -outfmt "6 qseqid sseq qlen length pident evalue"
blastn -task blastn -num_threads 4 -db Dasyurus_hallucatus_10X_01_style1raw.fasta -query sarHar1_twars.fa -out sarHar1-vs-quoll.blast.tsv -outfmt "6 qseqid sseq qlen length pident evalue"
blastn -task blastn -num_threads 4 -db Oros_1.0_HiC.fasta -query ailMel1_twars.fa -out ailMel1-vs-Oros.blast.tsv -outfmt "6 qseqid sseq qlen length pident evalue" &
blastn -task blastn -num_threads 4 -db Pteronura_brasiliensis_HiC.fasta -query ailMel1_twars.fa -out ailMel1-vs-ptebra.blast.tsv -outfmt "6 qseqid sseq qlen length pident evalue" &
blastn -task blastn -num_threads 4 -db VulVul2.2_HiC.fasta -query ailMel1_twars.fa -out -outfmt "6 qseqid sseq qlen length pident evalue"
blastn -task blastn -num_threads 4 -db meerkat_22Aug2017_6uvM2_HiC.fasta -query ailMel1_twars.fa -out -outfmt "6 qseqid sseq qlen length pident evalue"
blastn -task blastn -num_threads 4 -db aciJub1_HiC.fasta -query ailMel1_twars.fa -out -outfmt "6 qseqid sseq qlen length pident evalue"
blastn -task blastn -num_threads 4 -db Crocuta_crocuta_HiC.fasta -query ailMel1_twars.fa -out ailMel1-vs-croCro.blast.tsv -outfmt "6 qseqid sseq qlen length pident evalue"
```

#### Get best hits

```
awk '!seen[$1]++' sarHar1-vs-dunnart.blast.tsv > sarHar1-vs-dunnart.blast_bestHits.tsv/
awk '!seen[$1]++' sarHar1-vs-quoll.blast.tsv > sarHar1-vs-quoll.blast_bestHits.tsv/
awk '!seen[$1]++' ailMel1-vs-Oros.blast.tsv > ailMel1-vs-Oros.blast_bestHits.tsv/
awk '!seen[$1]++' ailMel1-vs-aciJub.blast.tsv > ailMel1-vs-aciJub.blast_bestHits.tsv/
awk '!seen[$1]++' ailMel1-vs-meerkat.blast.tsv > ailMel1-vs-meerkat.blast_bestHits.tsv/
awk '!seen[$1]++' ailMel1-vs-ptebra.blast.tsv > ailMel1-vs-ptebra.blast_bestHits.tsv/
awk '!seen[$1]++' ailMel1-vs-vulVul.blast.tsv > ailMel1-vs-vulVul.blast_bestHits.tsv/
awk '!seen[$1]++' ailMel1-vs-croCro.blast.tsv > ailMel1-vs-croCro.blast_bestHits.tsv/
awk '!seen[$1]++' ailMel1-vs-parHer.blast.tsv > ailMel1-vs-parHer.blast_bestHits.tsv/
```
### Align orthologous sequences and reconstruct ancestral sequences


### PREQUEL

*Compute marginal probability distributions for bases at ancestral nodes in a phylogenetic tree, using the tree model defined in tree.mod (may be produced with phyloFit).  These distributions are computed using the sum-product algorithm, assuming independence of sites.*

*Currently, indels are not treated probabilistically (hence the "largely") but are reconstructed by parsimony, also assuming site independence.  Specifically, each base is assumed to have been inserted on the branch leading to the last common ancestor (LCA) of all species that have actual bases (as opposed to alignment gaps or missing data) at a given site; gaps in descendant species are assumed to have arise (parsimoniously) from deletions.  When this LCA is either the left or right child of the root, insertions on one branch cannot be distinguished from deletions on the other.  We conservatively assume that the base was present at the root and was subsequently deleted. (Note that this will produce an upward bias on the length of the sequence at the root.)*

TWAR sequences aligned with PRANK first for both **Carnivora** and **Dasyuromorphia**:

```
prank -d=TWAR.fa -o=TWAR
```

Guide tree models taken from http://vertlife.org/phylosubsets//

**Carnivora Tree:**
>(((vulVul:7.8710407759,clup:7.87104049523)vulVul-clup:23.50144568,(((spiGra:23.2430925656,(taxTax:12.96099243,pteBra:12.960990938800002)taxTax-pteBra:10.282100137)spiGra-taxTax:2.441407474,odoRos:25.684500602)spiGra-odoRos:0.6828890173,ailMel1:26.36738793)spiGra-ailMel1:5.005098522)vulVul-spiGra:5.768964911,(((croCro:17.9867802921,meerkat:17.986781415000003)croCro-meerkat:2.37155057,parHer:20.358332829)croCro-parHer:1.970312193,aciJub:22.3286433317)croCro-aciJub:14.812807184)vulVul-croCro;/

**Dasyuromorphia Tree:**
>((Sminthopsis_crassicaudata:26.387672452,(Dasyurus_hallucatus:10.39305223,Sarcophilus_harrisii:10.393048932):15.994624624):8.481209642,Thylacinus_cynocephalus:34.868886865);/

Use phyloFit to generate tree models for each TWAR:

```
phyloFit --tree ../tree/carnivoratree.nwk TWAR.best.fas -o TWAR
phyloFit --tree ../tree/dasyuromorphia.nwk TWAR.best.fas -o TWAR
```

```
prequel -i FASTA -n -s vulVul-croCro TWAR_aligned.fa TWAR.mod anc
prequel -i FASTA -n -s smiCra-tcyn TWAR_aligned.fa TWAR.mod anc
```

Have to rename the output in between each one otherwise it saves over the top of the reconstruction:

```
mv anc.vulVul-croCro.fa TWAR_vulVul-croCro.fa
mv anc.smiCra-tcyn.fa TWAR_smiCra-tcyn.fa
```

#### Plot trees to check alignment of sequences

`scripts/plot_trees.R`

#### Sequence length comparisons and alignment gap counts
`scripts/twar_length.sh`



## Filter TWARs included in MPRA

TWARs were removed if one of the focal sequences was less than 90bp. This left 296 TWARs from a list of 339.
One TWAR was filtered out because it contained the BsiWI restriction enzyme site using for cloning. Also checked that the adapter sites were not present in the sequences.

## Tile TWARs and add adapter sequences
Sequences tiled using https://www.bioinformatics.org/sms2/split_fasta.html from the bioinformatics SMS2 tools.  

Sequences tiled to have a 170bp window size and a 15bp step size. From 296 TWARS x 6 orthologues = 1776 TWAR orthologues, end up with **10835 tiles**.

Check TWAR tiles for SfiI sites and add adapters to the end using the script:

`scripts/add_adapters.py`

Check for BsiWI and SfiI sites again after adding adapters

Removed: TWAR11.chr6 (all species), sarHar1_TWAR13.chr18, vulVul-croCro_TWAR4.chr12

## Generate random scrambled sequences for controls

Davide wrote a script to do this.
Generates random sequences and then filters them to exclude any that overlap with regions of the mm10 genome, mm10 TFBS motifs (MEME) or contain the restriction sites for BsiWI and SfiI.

```
module load Biopython/1.73-spartan_intel-2017.u2-Python-3.6.4
module load BLAT/3.5-intel-2016.u3
module load MEME/5.0.5-intel-2018.u4-Perl-5.26.2-Python-3.6.4
```

`scripts/random_sequences.py`

## Positive controls


The following sequences had a BsiWI restriction enzyme site and were altered from CGTACG to CGATCG:
- fragment_13;vista_mm901_start=601;end=770;length=170;source_length=1600
- fragment_14;vista_mm901_start=651;end=820;length=170;source_length=1600
- fragment_15;vista_mm901_start=701;end=870;length=170;source_length=1600
- fragment_1;runx2_popc_11_start=1;end=116;length=116;source_length=116
- fragment_16;vista_mm901_start=751;end=920;length=170;source_length=1600
- fragment_3;mm_TWAR5.chr15_start=21;end=190;length=170;source_length=208

fragment_8;dhs_popc_166:
- deleted CGTACGGTCTACC from end of last fragment for this region

Deleted the following controls as they contained too many BsiWI cut sites:
- dhs_popc_41
- dhs_popc_10
- dhs_popc_36

## Set up spreadsheet to send for synthesis

`scripts/convertFASTAtoTAB.py`
